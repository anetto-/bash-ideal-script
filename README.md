# Идеальный скрипт на bash

Пример идеального скрипта на bash. Он создаёт каталоги от N до N+10, но это не главное. Главное - он снабжён документацией, идиоматичной обработкой ошибок на bash, правильно структурирован и расширяем. [Подробнее на видео](https://www.youtube.com/watch?v=FuOgKi8XPkw)

# Bash ideal script example

This is example of bash ideal script. It creates dirs from N to N+10, but it is not main part. Main part - it is documented, has idiom error handling, well-formed and easy-to-modify. [More on video](https://www.youtube.com/watch?v=FuOgKi8XPkw)
